//
//  NavigationTransitionAnimate.h
//  TransitionAnimationOC
//
//  Created by near on 2017/10/17.
//  Copyright © 2017年 near. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavigationTransitionAnimate : NSObject<UIViewControllerAnimatedTransitioning>

@property (nonatomic,assign) BOOL isPush;

@end
