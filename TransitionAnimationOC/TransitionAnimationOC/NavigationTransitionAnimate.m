//
//  NavigationTransitionAnimate.m
//  TransitionAnimationOC
//
//  Created by near on 2017/10/17.
//  Copyright © 2017年 near. All rights reserved.
//

#import "NavigationTransitionAnimate.h"
@interface NavigationTransitionAnimate() 

@end
@implementation NavigationTransitionAnimate

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.8;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    UIViewController *fromVC = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toVC = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    UIView *containerView = [transitionContext containerView];
    UIView * fromView = fromVC.view;
    UIView * toView   = toVC.view;
    
    NSTimeInterval duration = [self transitionDuration:transitionContext];
    //把目标视图加入进去
    [containerView addSubview:toView];
    [containerView addSubview:fromView];
    if (self.isPush) {
        
        [containerView bringSubviewToFront:toView];
        toView.frame = CGRectOffset(containerView.bounds, CGRectGetWidth(containerView.bounds), 0);
        fromView.frame = containerView.bounds;
        
        [UIView animateWithDuration:duration animations:^{
            
            toView.frame = containerView.bounds;
//            fromView.transform = CGAffineTransformMakeScale(0.8, 0.9);
        } completion:^(BOOL finished) {
            
            fromView.transform = CGAffineTransformIdentity;
            BOOL wasCancelled = transitionContext.transitionWasCancelled;
            [transitionContext completeTransition:!wasCancelled];
        }];
    } else { //pop
        
        [containerView bringSubviewToFront:fromView];
        toView.frame = containerView.bounds;
//        toView.transform = CGAffineTransformMakeScale(0.8, 0.9);
        fromView.frame = containerView.bounds;
        [UIView animateWithDuration:duration animations:^{
            
            toView.frame = containerView.bounds;
            toView.transform = CGAffineTransformIdentity;
            fromView.frame = CGRectOffset(containerView.bounds, CGRectGetWidth(containerView.frame), 0);
        } completion:^(BOOL finished) {
            
            BOOL wasCancelled = transitionContext.transitionWasCancelled;
            [transitionContext completeTransition:!wasCancelled];
        }];
    }
    
//    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
//    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
//
//    //转场的容器图，动画完成之后会消失
//    UIView *containerView =  [transitionContext containerView];
//    UIView *fromView = nil;
//    UIView *toView = nil;
//
//    if ([transitionContext respondsToSelector:@selector(viewForKey:)]) {
//        fromView = [transitionContext viewForKey:UITransitionContextFromViewKey];
//        toView = [transitionContext viewForKey:UITransitionContextToViewKey];
//    }else{
//        fromView = fromViewController.view;
//        toView = toViewController.view;
//    }
//    //对应关系
//
//    if (YES) {
//        [containerView bringSubviewToFront:toView];
//        toView.frame = CGRectOffset(containerView.bounds, CGRectGetWidth(containerView.bounds), 0);
//        fromView.frame = containerView.bounds;
//    } else {
//        [containerView bringSubviewToFront:fromView];
//        toView.frame = containerView.bounds;
//        fromView.frame = containerView.bounds;
//    }
//
//
//    BOOL isPresent = (toViewController.presentingViewController == fromViewController);
//
//
//    NSTimeInterval transitionDuration = [self transitionDuration:transitionContext];
//
//    [UIView animateWithDuration:transitionDuration animations:^{
//        if (isPresent) {
//            toView.frame = containerView.bounds;
//            fromView.frame = CGRectOffset(containerView.bounds, containerView.bounds.size.width*0.3*-1, 0);
//        }
//    } completion:^(BOOL finished) {
//        BOOL isCancelled = [transitionContext transitionWasCancelled];
//
//        if (isCancelled)
//            [toView removeFromSuperview];
//
//        [transitionContext completeTransition:!isCancelled];
//    }];
}
@end
