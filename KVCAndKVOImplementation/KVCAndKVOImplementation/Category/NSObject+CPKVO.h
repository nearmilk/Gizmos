//
//  NSObject+CPKVO.h
//  KVOImplementation
//
//  Created by near on 2017/7/21.
//  Copyright © 2017年 near. All rights reserved.
//

#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN
/* 用于-addObserver的选项：forKeyPath：options：context：and -addObserver：toObjectsAtIndexes：forKeyPath：options：context：.
 */

typedef NS_OPTIONS(NSUInteger, CP_NSKeyValueObservingOptions) {
    /* 通知中发送的更改字典是否应分别包含NSKeyValueChangeNewKey和NSKeyValueChangeOldKey条目.
     */
    CP_NSKeyValueObservingOptionNew = 0x01,
    CP_NSKeyValueObservingOptionOld = 0x02,
    
    /* 在观察员注册方法甚至返回之前是否立即将通知发送给观察者。 如果还指定了NSKeyValueObservingOptionNew，则通知中的更改字典将始终包含NSKeyValueChangeNewKey条目，但不会包含NSKeyValueChangeOldKey条目。 （在初始通知中，观察到的属性的当前值可能是旧的，但对于观察者而言是新的。）您可以使用此选项，而不是显式调用，同时也由观察者的-observeValueForKeyPath调用的代码： ofObject：change：context：method。 当与-addObserver一起使用此选项时：toObjectsAtIndexes：forKeyPath：options：context：将为要添加观察者的每个已索引对象发送通知.
     */
    CP_NSKeyValueObservingOptionInitial NS_ENUM_AVAILABLE(10_5, 2_0) = 0x04,
    
    /* 在每次更改之前和之后，是否应将单独的通知发送给观察者，而不是在更改后单个通知。 更改前发送的通知中的更改字典总是包含一个NSKeyValueChangeNotificationIsPriorKey条目，其值为[NSNumber numberWithBool：YES]，但不包含NSKeyValueChangeNewKey条目。 当观察者自己的KVO合规性要求它为其自己的属性之一调用一个-willChange ...方法时，可以使用此选项，该属性的值取决于观察对象属性的值。 （在这种情况下，为了响应在更改后收到一个-observeValueForKeyPath：ofObject：change：context：message，可以正确地调用-willChange ...来处理太晚了。）
     
当指定此选项时，更改后发送的通知中的更改字典包含与未指定此选项时包含的相同条目，但由NSOrderedSets表示的排序唯一的多对多关系除外。 对于那些，对于NSKeyValueChangeInsertion和NSKeyValueChangeReplacement更改，更改通知的更改字典包含一个NSKeyValueChangeIndexesKey（在替换情况下为NSKeyValueChangeOldKey，其中在注册时指定了NSKeyValueObservingOptionOld选项），这些索引（和对象）可以将* *由操作改变。 更改后的第二个通知包含报告实际发生变化的条目。 对于NSKeyValueChangeRemoval更改，索引的删除是精确的.
     */
    CP_NSKeyValueObservingOptionPrior NS_ENUM_AVAILABLE(10_5, 2_0) = 0x08
};

/* 更改字典中NSKeyValueChangeKindKey条目中可能的值。 有关更多信息，请参阅-observeValueForKeyPath：ofObject：change：context：的注释.
 */
typedef NS_ENUM(NSUInteger, CP_NSKeyValueChange) {
    CP_NSKeyValueChangeSetting = 1,
    CP_NSKeyValueChangeInsertion = 2,
    CP_NSKeyValueChangeRemoval = 3,
    CP_NSKeyValueChangeReplacement = 4,
};

/* 与-willChangeValueForKey一起使用的可能种类的设置突变：withSetMutation：usingObjects：和-didChangeValueForKey：withSetMutation：usingObjects :. 他们的语义分别对应于NSMutableSet的-unionSet :, -minusSet :, -intersectSet :,和-setSet：method.
 */
typedef NS_ENUM(NSUInteger, CP_NSKeyValueSetMutationKind) {
    CP_NSKeyValueUnionSetMutationKind = 1,
    CP_NSKeyValueMinusSetMutationKind = 2,
    CP_NSKeyValueIntersectSetMutationKind = 3,
    CP_NSKeyValueSetSetMutationKind = 4,
};

typedef NSString * CP_NSKeyValueChangeKey NS_STRING_ENUM;
/* 更改字典中的条目的键。 有关更多信息，请参阅-observeValueForKeyPath：ofObject：change：context：的注释.
 */
FOUNDATION_EXPORT CP_NSKeyValueChangeKey const CP_NSKeyValueChangeKindKey;
FOUNDATION_EXPORT CP_NSKeyValueChangeKey const CP_NSKeyValueChangeNewKey;
FOUNDATION_EXPORT CP_NSKeyValueChangeKey const CP_NSKeyValueChangeOldKey;
FOUNDATION_EXPORT CP_NSKeyValueChangeKey const CP_NSKeyValueChangeIndexesKey;
FOUNDATION_EXPORT CP_NSKeyValueChangeKey const CP_NSKeyValueChangeNotificationIsPriorKey NS_AVAILABLE(10_5, 2_0);
/**
 @param observedObject  register observe object
 @param observedKey     observe object property's name
 @param oldValue        observed object property's old value
 @param newValue        observed object property's new value
 */
typedef void(^CPObservingBlock)(id observedObject, NSString *observedKey, id oldValue, id newValue);
@interface NSObject (CP_NSKeyValueObserving)

/* 假设接收方已经被注册为关于相对于对象的关键路径的值的观察者，则通知该值的变化.
 
 更改字典总是包含一个NSKeyValueChangeKindKey条目，其值为包装NSKeyValueChange（use - [NSNumber unsignedIntegerValue]）的NSNumber。 NSKeyValueChange的含义取决于关键路径识别的属性类型:
 - 对于任何类型的属性（属性，一对一关系，或有序或无序的多对多关系）NSKeyValueChangeSetting表示观察到的对象已收到一个-setValue：forKey：message，或者键值编码兼容集合方法 该键已被调用，或者一个-willChangeValueForKey：/ - didChangeValueForKey：pair已被调用.
 - 对于_ordered_ to-many关系，NSKeyValueChangeInsertion，NSKeyValueChangeRemoval和NSKeyValueChangeReplacement表示已将mutate消息发送到发送到对象的-mutableArrayValueForKey：消息返回的数组，或发送到由-mutableOrderedSetValueForKey：message返回的有序集 或发送到对象，或键值中的一个键值编码兼容的数组或有序的集合突变方法已被调用，或者一个-willChange：valuesAtIndexes：forKey：/ - didChange：valuesAtIndexes：forKey：pair已被调用.
 - 对于_unordered_ to-many关系（在Mac OS 10.4中引入），NSKeyValueChangeInsertion，NSKeyValueChangeRemoval和NSKeyValueChangeReplacement表示突变消息已经发送到发送到对象的-mutableSetValueForKey：消息返回的集合，或者该关键字之一 已经调用了用于密钥的值符合编码的集合突变方法，或者一个-willChangeValueForKey：withSetMutation：usingObjects：/ - didChangeValueForKey：withSetMutation：usingObjects：pair已被调用.
 
 对于任何类型的属性，如果在观察者注册时指定了NSKeyValueObservingOptionNew，则更改字典包含NSKeyValueChangeNewKey条目，这是正确的更改，这不是事先通知。 如果指定了NSKeyValueObservingOptionOld，则更改字典包含NSKeyValueChangeOldKey，它是正确的更改。 请参阅NSKeyValueObserverNotification非正式协议方法的注释，这些条目的值可以是.
 
 对于_ordered_ 一对多关系，更改字典总是包含一个NSKeyValueChangeIndexesKey条目，其值为包含插入，删除或替换对象的索引的NSIndexSet，除非更改为NSKeyValueChangeSetting.
 
 如果在观察员注册时指定了NSKeyValueObservingOptionPrior（在Mac OS 10.5中引入），并且此更改之前发送的通知是更改，则更改字典包含NSKeyValueChangeNotificationIsPriorKey条目，其值为NSNumber包装YES（use - [NSNumberboolValue]）.
 
 上下文始终是在观察者注册时传递的指针.
 */
- (void)cp_observeValueForKeyPath:(nullable NSString *)keyPath ofObject:(nullable id)object change:(nullable NSDictionary<CP_NSKeyValueChangeKey, id> *)change context:(nullable void *)context;

/**
 Add observation listening for the object
 @param observer register observe object
 @param keyPath ww
 @param block ww
 */
- (void)cp_addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath block:(CPObservingBlock)block;//zidingyi

/**
 Remove specified monitoring
 @param observer register observe object
 @param keyPath ww
 */
//- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath;//zidingyi
@end

@interface NSObject (CP_NSKeyValueObserverRegistration)

/* 在相对于接收器的关键路径上注册或注销作为值的观察者。 这些选项确定观察者通知中包含的内容以及发送的内容，如上所述，并且上下文如上所述在观察者通知中传递。 您应该使用-removeObserver：forKeyPath：context：而不是-removeObserver：forKeyPath：只要有可能，因为它允许您更精确地指定您的意图。 当同一个观察者多次注册相同的密钥路径时，每次都会使用不同的上下文指针，-removeObserver：forKeyPath：在确定要删除的内容时，必须猜测上下文指针，并且可以猜测错误.
 */
- (void)cp_addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(CP_NSKeyValueObservingOptions)options context:(nullable void *)context;
- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath context:(nullable void *)context NS_AVAILABLE(10_7, 5_0);
- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath;

@end

@interface NSArray<ObjectType> (CP_NSKeyValueObserverRegistration)

/* 在关键路径上注册或注销作为数组的每个索引元素的值的观察者。 这些选项确定观察者通知中包含的内容以及发送的内容，如上所述，并且上下文如上所述在观察者通知中传递。 这些不仅仅是方便的方法; 调用它们可能比重复调用NSObject（NSKeyValueObserverRegistration）方法要快得多。 您应该使用-removeObserver：fromObjectsAtIndexes：forKeyPath：context：而不是-removeObserver：fromObjectsAtIndexes：forKeyPath：只要可能，NSObject（NSKeyValueObserverRegistration）注释中描述的原因相同.
 */
- (void)cp_addObserver:(NSObject *)observer toObjectsAtIndexes:(NSIndexSet *)indexes forKeyPath:(NSString *)keyPath options:(CP_NSKeyValueObservingOptions)options context:(nullable void *)context;
- (void)cp_removeObserver:(NSObject *)observer fromObjectsAtIndexes:(NSIndexSet *)indexes forKeyPath:(NSString *)keyPath context:(nullable void *)context NS_AVAILABLE(10_7, 5_0);
- (void)cp_removeObserver:(NSObject *)observer fromObjectsAtIndexes:(NSIndexSet *)indexes forKeyPath:(NSString *)keyPath;

/* NSArrays不可观察，所以这些方法在NSArrays上调用时引发异常。 观察数组，而不是观察数组是相关对象集合的有序对数关系.
 */
- (void)cp_addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(CP_NSKeyValueObservingOptions)options context:(nullable void *)context;
- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath context:(nullable void *)context NS_AVAILABLE(10_7, 5_0);
- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath;
@end

@interface NSOrderedSet<ObjectType>(CP_NSKeyValueObserverRegistration)

/* NSOrderedSets是不可观察的，所以这些方法在NSOrderedSets调用时引发异常。 观察有序集合，而不是观察有序集合是有关对象集合的有序多对象关系.
 */
- (void)cp_addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(nullable void *)context;
- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath context:(nullable void *)context NS_AVAILABLE(10_7, 5_0);
- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath;

@end
@interface NSSet<ObjectType>(CP_NSKeyValueObserverRegistration)

/* NSSets是不可观察的，所以这些方法在NSSet调用时引发异常。 观察集合，而不是观察该集合是相关对象的集合的无序多对象关系.
 */
- (void)cp_addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath options:(NSKeyValueObservingOptions)options context:(nullable void *)context;
- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath context:(nullable void *)context NS_AVAILABLE(10_7, 5_0);
- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath;

@end

@interface NSObject(CP_NSKeyValueObserverNotification)

/* 给定一个标识属性（属性，一对一关系或有序或无序的多对多关系）的键，将-KeyValueForKeyPath：ofObject：change：context：类型为NSKeyValueChangeSetting的通知消息发送给注册为密钥的每个观察者，包括 使用在该对象中定位键控值的键路径向其他对象注册。 这些方法的调用必须始终配对.
 
 使用这些方法产生的通知中的更改字典包含可选项，如果在观察员注册时要求:
 - NSKeyValueChangeOldKey条目（如果存在）包含-valueForKey返回的值：在调用-willChangeValueForKey：（或NSNull if -valueForKey：返回 nil）的时刻，.
 - NSKeyValueChangeNewKey条目（如果存在）包含-valueForKey返回的值：在-didChangeValueForKey：被调用的时刻（或NSNull if -valueForKey：返回 nil）.
 */
- (void)cp_willChangeValueForKey:(NSString *)key;
- (void)cp_didChangeValueForKey:(NSString *)key;

/* 给定一个标识_ordered_ 一对多关系的键，发送-observeValueForKeyPath：ofObject：change：context：传入更改类型的通知消息给注册为密钥的每个观察者，包括使用关键路径注册到其他对象的那些 可以在此对象中找到键控值。 传入的类型必须是NSKeyValueChangeInsertion，NSKeyValueChangeRemoval或NSKeyValueChangeReplacement。 传入索引集必须是要插入，删除或替换的对象的索引。 这些方法的调用必须始终配对，具有相同的参数.
 
 使用这些方法产生的通知中的更改字典包含可选项，如果在观察员注册时要求:
 - NSKeyValueChangeOldKey条目（仅适用于NSKeyValueChangeRemoval和NSKeyValueChangeReplacement）包含由-valueForKey返回的数组中的索引对象的数组：在-willChangeValueForKey：valuesAtIndexes：forKey：被调用的时刻.
 - NSKeyValueChangeNewKey条目（仅适用于NSKeyValueChangeInsertion和NSKeyValueChangeReplacement）包含由-valueForKey返回的数组中的索引对象数组：-didChangeValueForKey：valuesAtIndexes：forKey：被调用的时间.
 */
- (void)cp_willChange:(NSKeyValueChange)changeKind valuesAtIndexes:(NSIndexSet *)indexes forKey:(NSString *)key;
- (void)cp_didChange:(NSKeyValueChange)changeKind valuesAtIndexes:(NSIndexSet *)indexes forKey:(NSString *)key;

/* 给出一个标识_unordered_ 一对多关系的键，将-observeValueForKeyPath：ofObject：change：context：notification消息发送给注册为密钥的每个观察者，包括使用在其中定位键控值的关键路径向其他对象注册的那些 目的。 传入突变种类对应于NSMutableSet方法。 传入集必须包含将传递给相应的NSMutableSet方法的集合。 这些方法的调用必须始终配对，具有相同的参数.
 
 在使用这些方法的通知中，更改字典中的NSKeyValueChangeKindKey条目的值取决于传入的突变值:
 - NSKeyValueUnionSetMutation -> NSKeyValueChangeInsertion
 - NSKeyValueMinusSetMutation -> NSKeyValueChangeRemoval
 - NSKeyValueIntersectSetMutation -> NSKeyValueChangeRemoval
 - NSKeyValueSetSetMutation -> NSKeyValueChangeReplacement
 
 更改字典也可能包含可选条目:
 - NSKeyValueChangeOldKey条目（如果存在）（仅适用于NSKeyValueChangeRemoval和NSKeyValueChangeReplacement），包含已删除的对象集.
 - NSKeyValueChangeNewKey条目（如果存在）（仅适用于NSKeyValueChangeInsertion和NSKeyValueChangeReplacement）包含添加的对象集.
 */
- (void)cp_willChangeValueForKey:(NSString *)key withSetMutation:(NSKeyValueSetMutationKind)mutationKind usingObjects:(NSSet *)objects;
- (void)cp_didChangeValueForKey:(NSString *)key withSetMutation:(NSKeyValueSetMutationKind)mutationKind usingObjects:(NSSet *)objects;

@end

@interface NSObject(CP_NSKeyValueObservingCustomization)

/* 返回一组关键路径，用于其值影响键控属性值的属性。 当密钥的观察者注册到接收类的实例时，KVO本身自动观察同一实例的所有密钥路径，并且当这些密钥路径中的任何一个的值改变时，将密钥的更改通知发送给观察者。 此方法的默认实现将在接收类中搜索其名称与模式+ keyPathsForValuesAffecting <Key>匹配的方法，并返回调用该方法的结果（如果找到）。 所以，任何这样的方法都必须返回NSSet。 如果没有找到这样的方法，则返回根据以前调用现在被弃用的+ setKeys：triggerChangeNotificationsForDependentKey：方法提供的信息计算出的NSSet，用于向后的二进制兼容性.
 
 这种方法和KVO的自动使用包括一个依赖机制，您可以使用它，而不是发送-willChangeValueForKey：/ - didChangeValueForKey：依赖，计算的属性的消息.
 
 当您的其中一个属性的getter方法使用其他属性的值（包括由密钥路径定位的值）计算要返回的值时，可以覆盖此方法。 您的覆盖应通常调用超级，并返回一个集合，该集合包含集合中由此执行的任何成员（从而不会干扰超类中覆盖此方法）.
 
 当您使用类别将计算属性添加到现有类中时，您不能重写此方法，因为您不应该覆盖类别中的方法。 在这种情况下，实现匹配的+ keyPathsForValuesAffecting <Key>来利用这一机制.
 */
+ (NSSet<NSString *> *)cp_keyPathsForValuesAffectingValueForKey:(NSString *)key NS_AVAILABLE(10_5, 2_0);

/* 如果键值观察机器应自动调用，则返回YES -willChangeValueForKey：/ - didChangeValueForKey :, -willChange：valuesAtIndexes：forKey：/ - didChange：valuesAtIndexes：forKey：或-willChangeValueForKey：withSetMutation：usingObjects：/ - didChangeValueForKey：withSetMutation： usingObjects：每当该类的实例接收密钥的密钥值编码消息，或者调用密钥的突变键值编码兼容方法。 否则返回否。 从Mac OS 10.5开始，此方法的默认实现将在接收类中搜索其名称与模式+ automationNotifiesObserversOf <Key>匹配的方法，并返回调用该方法的结果（如果找到）。 所以，任何这样的方法也必须返回BOOL。 如果没有找到这样的方法，则返回YES.
 */
+ (BOOL)cp_automaticallyNotifiesObserversForKey:(NSString *)key;

/* 获取或返回一个指针，用于标识所有在接收者注册的观察者的信息，在注册时使用的选项等。这些方法的默认实现将观察信息存储在由接收者的“指针。 为了提高性能，您可以覆盖这些方法来将不透明数据指针存储在实例变量中。 覆盖这些方法不得尝试将Objective-C消息发送到传入观察信息，包括-retain和-release.
 */
@property (nullable) void *observationInfo NS_RETURNS_INNER_POINTER;

@end

#if (TARGET_OS_MAC && !(TARGET_OS_EMBEDDED || TARGET_OS_IPHONE))

@interface NSObject(CP_NSDeprecatedKeyValueObservingCustomization)

/* 在Mac OS 10.5中弃用的方法，有利于使用+ keyPathsForValuesAffectingValueForKey：。 注册调用-willChangeValueForKey：/ - didChangeValueForKey :, -willChange：valuesAtIndexes：forKey：/ - didChange：valuesAtIndexes：forKey :,和-willChangeValueForKey：withSetMutation：usingObjects：/ - didChangeValueForKey：withSetMutation：usingObjects：用于任何键 传入数组还应该发送依赖关键字的通知.
 */
+ (void)cp_setKeys:(NSArray *)keys triggerChangeNotificationsForDependentKey:(NSString *)dependentKey NS_DEPRECATED(10_0, 10_5, 2_0, 2_0);

@end

#endif
NS_ASSUME_NONNULL_END
