//
//  NSObject+CPKVO.m
//  KVOImplementation
//
//  Created by near on 2017/7/21.
//  Copyright © 2017年 near. All rights reserved.
//

#import "NSObject+CPKVO.h"
#import <objc/runtime.h>
#import <objc/message.h>

NSString *const kCPKVOClassPrefix = @"CPKVOClassPrefix_";
NSString *const kCPKVOAssociatedObservers = @"CPKVOAssociatedObservers";

#pragma mark - ObservationInfo

@interface CPObservationInfo : NSObject

@property (nonatomic, weak) NSObject *observer;
@property (nonatomic, copy) NSString *keyPath;
@property (nonatomic, copy) CPObservingBlock block;
@end

@implementation CPObservationInfo

- (instancetype)initWithObserver:(NSObject *)observer keyPath:(NSString *)keyPath block:(CPObservingBlock)block{
    self = [super init];
    if (!self) return nil;
    
    _observer = observer;
    _keyPath = keyPath;
    _block = block;
    
    return self;
}
@end

#pragma mark - Helps methods
static NSString *getterForSetter(NSString *setter){
    if (setter.length <= 0 || ![setter hasPrefix:@"set"] || ![setter hasSuffix:@":"]) {
        return nil;
    }
    
    //remove 'set'and ':'
    NSRange range = NSMakeRange(3, setter.length - 4);
    NSString *keyPath = [setter substringWithRange:range];
    
    //lower first letter
    NSString *firstLetter = [[keyPath substringToIndex:1] lowercaseString];
    
    //replace first letter
    keyPath = [keyPath stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:firstLetter];
    
    return keyPath;
}
/**
 Return setter name or nil if name length <= 0
 */
static NSString *setterForGetter(NSString *getter){
    if (getter.length <= 0) {
        return nil;
    }
    
    // upper case the first letter
    NSString *firstLetter = [[getter substringToIndex:1] uppercaseString];
    NSString *remainingLetters = [getter substringFromIndex:1];
    
    NSString *setter = [NSString stringWithFormat:@"set%@%@:",firstLetter,remainingLetters];
    return setter;
}

static Class kvo_class(id self, SEL _cmd){
   return class_getSuperclass(object_getClass(self));
}

static void kvo_setter(id self, SEL _cmd, id newValue){
    NSString *setterName = NSStringFromSelector(_cmd);
    NSString *getterName = getterForSetter(setterName);
    
    if (!getterName) {
        NSString *reason = [NSString stringWithFormat:@"Object %@ does not have setter %@",self, setterName];
        @throw [NSException exceptionWithName:NSInvalidArgumentException reason:reason userInfo:nil];
    }
    
    //KVC
    id oldValue = [self valueForKey:getterName];
    
    struct objc_super superclazz = {
        .receiver = self,
        .super_class = class_getSuperclass(object_getClass(self))
    };
    
    void (*objc_msgSendSuperCasted)(void *, SEL, id) = (void *)objc_msgSendSuper;
    
    objc_msgSendSuperCasted(&superclazz, _cmd, newValue);
    
    NSMutableArray *observers = objc_getAssociatedObject(self, (__bridge const void *)(kCPKVOAssociatedObservers));
    
    for (CPObservationInfo *info in observers) {
        if ([info.keyPath isEqualToString:getterName]) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                info.block(self, getterName, oldValue, newValue);
            });
        }
    }
}




#pragma mark - NSObject Category CPKVO
@implementation NSObject (CPKVO)

#pragma mark - Public method
- (void)cp_addObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath block:(CPObservingBlock)block{
    // Step 1: Throw exception if class or superClass doesn't implement the setter
    SEL setterSel = NSSelectorFromString(setterForGetter(keyPath));
    Method setterMethod = class_getInstanceMethod([self class], setterSel);
    if (!setterMethod) {
        NSString *reason = [NSString stringWithFormat:@"Object %@ does not have a setter for key %@",self,keyPath];
        //thorws exception
        @throw [NSException exceptionWithName:NSInvalidArgumentException reason:reason userInfo:nil];
        return;
    }
    Class clazz = object_getClass(self);
    NSString *clazzName = NSStringFromClass(clazz);
    
    //if not an KVO class
    if (![clazzName hasPrefix:kCPKVOClassPrefix]) {
        //point isa to the KVO class
        clazz = [self makeKvoClassWithOriginalClassName:clazzName];
        //set object to clazz
        object_setClass(self, clazz);
    }
    //add our kvo setter if this kvo class(not superclasses) doesn't implement the setter
    if (![self hasSelector:setterSel]) {
        const char  *types = method_getTypeEncoding(setterMethod);
        class_addMethod(clazz, setterSel, (IMP)kvo_setter, types);
    }
    
    //init an observer info
    CPObservationInfo *info = [[CPObservationInfo alloc]initWithObserver:observer keyPath:keyPath block:block];
    
    //get the associated object
    NSMutableArray *observers = objc_getAssociatedObject(self, (__bridge const void *)(kCPKVOAssociatedObservers));
    
    //set associated object if associated object does not exist
    if (!observer) {
        observers = [NSMutableArray new];
        objc_setAssociatedObject(self, (__bridge const void *)(kCPKVOAssociatedObservers), observers, OBJC_ASSOCIATION_RETAIN);
    }
    [observers addObject:info];
}

- (void)cp_removeObserver:(NSObject *)observer forKeyPath:(NSString *)keyPath{
    NSMutableArray *observers = objc_getAssociatedObject(self, (__bridge const void *)kCPKVOAssociatedObservers);
    CPObservationInfo *infoRemove;
    for (CPObservationInfo *info in observers) {
        if (info.observer == observer && [info.keyPath isEqual:keyPath]) {
            infoRemove = info;
            break;
        }
    }
    
    [observers removeObject:infoRemove];
}

#pragma mark - Private method
- (Class)makeKvoClassWithOriginalClassName:(NSString *)originalClassName{
    //add prefix
    NSString *kvoClazzName =[kCPKVOClassPrefix stringByAppendingString:originalClassName];
    
    //if find the specified class, reurn this class
    Class clazz = NSClassFromString(kvoClazzName);
    if (clazz) {
        return clazz;
    }
    
    // class doesn't exist,create it
    Class originalClazz = NSClassFromString(originalClassName);
    
    // create KVO class ,it's super class is self(originalClazz)
    Class kvoClazz = objc_allocateClassPair(originalClazz, kvoClazzName.UTF8String, 0);
    
    //i don't known
    Method clazzMethod = class_getInstanceMethod(originalClazz, @selector(class));
    const char *types = method_getTypeEncoding(clazzMethod);
    class_addMethod(kvoClazz, @selector(class), (IMP)kvo_class, types);
    
    //register this class
    objc_registerClassPair(kvoClazz);
    
    return kvoClazz;
}

/**
 if have this selector, return YES
 */
- (BOOL)hasSelector:(SEL)selector{
    Class clazz = object_getClass(self);
    
    unsigned int methodCount = 0;
    Method *methodList = class_copyMethodList(clazz, &methodCount);
    for (unsigned int i = 0; i < methodCount; i++) {
        SEL thisSelector = method_getName((Method) methodList[i]);
        if (thisSelector == selector) {
            free(methodList);
            return YES;
        }
    }
    free(methodList);
    
    return NO;
}
@end

