//
//  NSObject+CPKVC.m
//  KVCImplementation
//
//  Created by near on 2017/7/24.
//  Copyright © 2017年 near. All rights reserved.
//

#import "NSObject+CPKVC.h"
#import <objc/runtime.h>
#import <objc/message.h>
#import <CoreGraphics/CoreGraphics.h>
@implementation NSObject (CPKVC)

#pragma mark - Private Method

//在不考虑可能多层调用导致增加耗时的情况下
//将key转换为set方法名称
- (NSString *)convertToSetWithKey:(NSString *)key{
    
    NSString *setKey = [NSString stringWithFormat:@"set%@:",[self convertLetterCapitalized:key]];
    return setKey;
}
//将key转换为一组符合要求的名字来查找实例变量
- (NSArray<NSString *> *)convertKeyToInstanceVariable:(NSString *)key{
    NSString *_key = [NSString stringWithFormat:@"_%@",key];
    NSString *_isKey = [NSString stringWithFormat:@"_is%@",[self convertLetterCapitalized:key]];
    NSString *isKey = [NSString stringWithFormat:@"is%@",[self convertLetterCapitalized:key]];
    NSArray *array = @[_key,_isKey,key,isKey];
    
    return array;
}

//将key转换为一组符合valueForKey步骤一要求的名字来查找实例方法
- (NSArray<NSString *> *)convertGetKeyToStepOneInstanceMethod:(NSString *)key{
    NSString *getKey = [NSString stringWithFormat:@"get%@",[self convertLetterCapitalized:key]];
    NSString *isKey = [NSString stringWithFormat:@"is%@",[self convertLetterCapitalized:key]];
    NSArray *array = @[getKey,key,isKey];
    
    return array;
}

- (NSString *)convertLetterCapitalized:(NSString *)key{
    NSString *firstLetter = [key substringToIndex:1].uppercaseString;//首字母大写
    NSString *remainingLetters = [key substringFromIndex:1];
    
    return [NSString stringWithFormat:@"%@%@",firstLetter,remainingLetters];
}

/** 将value拆箱调用
 *  @param type value的类型
 *  @param value 一个传入值
 *  @param sel a sel
 */
//这里转换有点问题,还不知道如何动态转换NSValue的objcType的
- (void)splitOpenCase:(char *)type value:(id)value sel:(SEL)sel{
    //这里用的指定基本数据类型,当符合 -set<Key>:,但是参数类型与原本属性数据类型不同的话这边发送消息不知道会发生什么.
    switch (*type) {
        case 'c':{
            char aChar = [value charValue];
            void(*cp_objc_msgSend)(id,SEL,char) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,aChar);    return ;
        }
        case 'C':
        {
            unsigned char unsignedChar = (unsigned char)[value charValue];
            void(*cp_objc_msgSend)(id,SEL,unsigned char) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,unsignedChar);    return ;
        }
        case 's':
        {
            short aShort = [value shortValue];
            void(*cp_objc_msgSend)(id,SEL,short) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,aShort);    return ;
        }
        case 'i':
        {
            int aInt = [value intValue];
            void(*cp_objc_msgSend)(id,SEL,int) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,aInt);    return ;
        }
        case 'I':
        {
            unsigned int unsignedInt = (unsigned int)[value intValue];
            void(*cp_objc_msgSend)(id,SEL,unsigned int) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,unsignedInt);    return ;
        }
            //这里我不太知道long 和long long的在这里的关系我用long先
        case 'q':
        {
            long aLong= [value longValue];
            void(*cp_objc_msgSend)(id,SEL,long) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,aLong);    return ;
        }
        case 'Q':
        {
            unsigned long unsignedLong = (unsigned long)[value longValue];
            void(*cp_objc_msgSend)(id,SEL,unsigned long) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,unsignedLong);    return ;
        }
        case 'f':
        {
            float aFloat = [value floatValue];
            void(*cp_objc_msgSend)(id,SEL,float) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,aFloat);    return ;
        }
        case 'd':
        {
            double aDouble = [value doubleValue];
            void(*cp_objc_msgSend)(id,SEL,double) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,aDouble);    return ;
        }
        case 'B':
        {
            BOOL aBool = [value boolValue];
            void(*cp_objc_msgSend)(id,SEL,BOOL) = (void *)objc_msgSend;
            cp_objc_msgSend(self,sel,aBool);    return ;
        }
        default:{
            //NSValue
            
//            CGRect rect;
//            NSValue *avalue = (NSValue *)value;
//            NSLog(@"%s",avalue.objCType);
//            [value getValue:&rect];
//            void(*cp_objc_msgSend)(id,SEL,CGRect) = (void *)objc_msgSend;
//            cp_objc_msgSend(self,sel,rect);    return ;
        }
    }
}

/** 返回一个NSNumber类型
 *  @param type value的类型
 *  @param sel a sel
 *  @return 返回一个装箱的NSNumber,不可转换的返回nil
 */
- (NSNumber *)packing:(char *)type sel:(SEL)sel{
    NSNumber *value = nil;
    switch (*type) {
        case 'c':{
            char (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithChar:cp_objc_msgSend(self,sel)];
        }
            break;
        case 'C':
        {
            unsigned char (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithUnsignedChar:cp_objc_msgSend(self,sel)];
        }
            break;
        case 's':
        {
            short (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithShort:cp_objc_msgSend(self,sel)];
        }
            break;
        case 'i':
        {
            int (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithInt:cp_objc_msgSend(self,sel)];
        }
            break;
        case 'I':
        {
            unsigned int (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithUnsignedInt:cp_objc_msgSend(self,sel)];
        }
            break;
            //这里我不太知道long 和long long的在这里的关系我用long先
        case 'q':
        {
            long (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithInteger:cp_objc_msgSend(self,sel)];
        }
            break;
        case 'Q':
        {
            unsigned long (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithUnsignedInteger:cp_objc_msgSend(self,sel)];
        }
            break;
        case 'f':
        {
            float (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithFloat:cp_objc_msgSend(self,sel)];
        }
            break;
        case 'd':
        {
            double (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithDouble:cp_objc_msgSend(self,sel)];
        }
            break;
        case 'B':
        {
            BOOL (*cp_objc_msgSend)(id,SEL) = (void *)objc_msgSend;
            value = [NSNumber numberWithBool:cp_objc_msgSend(self,sel)];
        }
            break;
        default:
            break;
    }
    return value;
}
#pragma mark - Public Method
+ (BOOL)cp_accessInstanceVariableDirectly{
    return YES;
}

- (void)cp_setValue:(id)value forKey:(NSString *)key{
    //检查key是否为空字符串
    if (key == nil || key.length <= 0) {
        [self cp_setValue:value forUndefinedKey:key];
    }
    //获取当前实例的类
    //第一步:搜索类中是否有 -set<Key>: 的方法.如果有此方法，检查对象是否为一个对象指针类型，不是的话参数类型不能为nil,否则调用 -cp_setNilValueForKey:.将非指针对象的参数用 NSNumber/NSValue转换,调用此方法;否则执行第二步.
    Class clazz = object_getClass(self);
    SEL setterSEL = NSSelectorFromString([self convertToSetWithKey:key]);
    Method setterMethod = class_getInstanceMethod(clazz, setterSEL);
    if (setterMethod) {
        //检查方法,系统的好像即使返回类型不是void的是其它返回类型只要满足 -set<Key>也会调用，下面的同理.所以在这里我只检查后面的参数类型(好像只要不是@的在设置为nil的时候都调用 -cp_setNilValueForKey:).
        char *type = method_copyArgumentType(setterMethod, 2);
        if (*type != '@') {
            if (value == nil) {
                [self cp_setNilValueForKey:key]; return;
            }
            [self splitOpenCase:type value:value sel:setterSEL];
            return;
        }
        void(*cp_objc_msgSend)(id,SEL,id) = (void *)objc_msgSend;
        cp_objc_msgSend(self,setterSEL,value);
    }else{
        //第二步:如果accessInstanceVariablesDirectly为NO,执行 -cp_setValue:forUndefinedKey:,YES按_<key>, _is<Key>, <key>, 或者 is<Key>相匹配的实例变量查找,否则进行第三步.
        if (![clazz accessInstanceVariablesDirectly]) {
            [self cp_setValue:value forUndefinedKey:key]; return;
        }
        
        unsigned int ivarCount = 0;
        //返回类中所有实例变量
        Ivar *ivars = class_copyIvarList(clazz, &ivarCount);
        //返回符合要求名字的字符串数组
        NSArray<NSString *> *keyNameArray = [self convertKeyToInstanceVariable:key];
        //遍历ivars数组中再跟符合要求名字的数组中元素一一比对
        for (unsigned int i = 0; i < ivarCount; i++) {
            const char *ivarCName = ivar_getName(ivars[i]);
            NSString *ivarName = [NSString stringWithCString:ivarCName encoding:NSUTF8StringEncoding];
            
            for (NSUInteger j = 0; j < keyNameArray.count; j++) {
                if ([ivarName isEqualToString:keyNameArray[j]]) {
                    //找到符合要求的ivar，根据对象类型进行相应操作
                    const char *ivarType = ivar_getTypeEncoding(ivars[i]);
                    if (*ivarType != '@') {
                        if (value == nil) {
                            free(ivars); ivars = NULL;
                            [self cp_setNilValueForKey:key];
                            return;
                        }
                        //code
                        //endcode
                    }
                    object_setIvar(self, ivars[i], value);
                    free(ivars); ivars = NULL; return;
                }
            }
        }
        //第三步:相匹配的实例变量也找不到,调用 -cp_setValue:forUndefinedKey:.
        free(ivars); ivars = NULL;
        [self cp_setValue:value forUndefinedKey:key];
    }
}


- (void)cp_setValuesForKeysWithDictionary:(NSDictionary<NSString *,id> *)keyedValues{
    //遍历指定字典
    [keyedValues enumerateKeysAndObjectsUsingBlock:^(NSString * _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if (!stop) {//当遍历没有停止时，根据KVC给模型中属性赋值
            // key:用来给哪个属性
            // obj:给指定key属性的值
            [self cp_setValue:obj forKey:key];
        }
    }];
}

//这个方法的步骤有点多，六步骤
- (id)cp_valueForKey:(NSString *)key{
    //检查key是否为空字符串
    if (key == nil || key.length <= 0) {
        [self cp_valueForUndefinedKey:key];
    }
    //获取当前实例的类
    //第一步:搜索类中是否有 -set<Key>: 的方法.如果有此方法，检查对象是否为一个对象指针类型，不是的话参数类型不能为nil,否则调用 -cp_setNilValueForKey:.将非指针对象的参数用 NSNumber/NSValue转换,调用此方法;否则执行第二步.
    Class clazz = object_getClass(self);
    
    unsigned int methodCount = 0;
    Method *methods = class_copyMethodList(clazz, &methodCount);
    if (methods) {
        for (unsigned int i = 0; i < methodCount; i++) {
            SEL methodSel = method_getName(methods[i]);
            NSString *methodName = NSStringFromSelector(methodSel);
            //步骤一:查找符合-get<Key>,-<key>,或者 -is<Key>的，判断其方法结果的类型做相应操作
            NSArray<NSString *> *stepOneArray = [self convertGetKeyToStepOneInstanceMethod:key];
            for (NSUInteger stepOne = 0; stepOne < stepOneArray.count; stepOne++) {
                if ([methodName isEqualToString:stepOneArray[stepOne]]) {
                    char *returnType = method_copyReturnType(methods[i]);
                    NSNumber *value = [self packing:returnType sel:methodSel];
                    if (!value) {
                        //如果为结构体
                    }
                    return value;
                }
            }
            //TODO:集合相关验证还搞不懂先不写
//            //步骤二:
//            NSArray<NSString *> *stepTwoArray = @[];
//            for (NSUInteger stepTwo = 0; stepTwo < stepTwoArray.count; stepTwo++) {
//                if ([methodName isEqualToString:stepTwoArray[stepTwo]]) {
//                    char *returnType = method_copyReturnType(methods[i]);
//                    if (*returnType == '@') {//returnType为对象指针类型
//                        return [self performSelector:methodSel];
//                    }
//                    //code
//                    return nil;
//                }
//            }
//            //步骤三:
//            NSArray<NSString *> *stepThreeArray = @[];
//            for (NSUInteger stepThree = 0; stepThree < stepThreeArray.count; stepThree++) {
//                if ([methodName isEqualToString:stepThreeArray[stepThree]]) {
//                    char *returnType = method_copyReturnType(methods[i]);
//                    if (*returnType == '@') {//returnType为对象指针类型
//                        return [self performSelector:methodSel];
//                    }
//                    //code
//                    return nil;
//                }
//            }
//            //步骤四:
//            NSArray<NSString *> *stepFourArray = @[];
//            for (NSUInteger stepFour = 0; stepFour < stepFourArray.count; stepFour++) {
//                if ([methodName isEqualToString:stepFourArray[stepFour]]) {
//                    char *returnType = method_copyReturnType(methods[i]);
//                    if (*returnType == '@') {//returnType为对象指针类型
//                        return [self performSelector:methodSel];
//                    }
//                    //code
//                    return nil;
//                }
//            }
        }
        //步骤五:判断accessInstanceVariablesDirectly值,然后做是否搜寻实例变量操作
        if (![self.class cp_accessInstanceVariableDirectly]) {
            [self cp_valueForUndefinedKey:key];
        }
        unsigned int ivarCount = 0;
        //返回类中所有实例变量
        Ivar *ivars = class_copyIvarList(clazz, &ivarCount);
        //返回符合要求名字的字符串数组
        NSArray<NSString *> *keyNameArray = [self convertKeyToInstanceVariable:key];
        //遍历ivars数组中再跟符合要求名字的数组中元素一一比对
        for (unsigned int i = 0; i < ivarCount; i++) {
            const char *ivarCName = ivar_getName(ivars[i]);
            NSString *ivarName = [NSString stringWithCString:ivarCName encoding:NSUTF8StringEncoding];
            
            for (NSUInteger j = 0; j < keyNameArray.count; j++) {
                if ([ivarName isEqualToString:keyNameArray[j]]) {
                    //找到符合要求的ivar，根据对象类型进行相应操作
                    const char *ivarType = ivar_getTypeEncoding(ivars[i]);
                    if (*ivarType != '@') {
                        //code
                        //endcode
                    }
                    //指针对象
                    id ivarValue = object_getIvar(self, ivars[i]);
                    free(ivars); ivars = NULL;
                    return ivarValue;
                }
            }
        }
        free(ivars); ivars = NULL;
        [self cp_valueForUndefinedKey:key];
    }
    return nil;
}

//static OS_ALWAYS_INLINE
//Ivar _object_setInstanceVariable(id obj,const char *name, void *value, bool assumeStrong){
//    Ivar ivar = nil;
//    class_getv
//    if (obj && name && !obj->isTaggedPointer()) {
//        
//    }
//}
- (NSMutableSet *)cp_mutableSetValueForKey:(NSString *)key{
    return nil;
}

- (NSMutableArray *)cp_mutableArrayValueForKey:(NSString *)key{
    return nil;
}

- (NSMutableOrderedSet *)cp_mutableOrderedSetValuesForKey:(NSString *)key{
    return nil;
}

- (NSDictionary<NSString *,id> *)cp_dictionaryWithValuesForKeys:(NSArray<NSString *> *)keys{
    return nil;
}

- (BOOL)cp_validateValue:(inout id  _Nullable __autoreleasing *)ioValue forKey:(NSString *)inKey error:(out NSError * _Nullable __autoreleasing *)outError{
    return nil;
}

- (void)cp_setValue:(id)value forKeyPath:(NSString *)keyPath{
    
}

- (id)cp_valueForKeyPath:(NSString *)keyPath{
    return nil;
}

- (NSMutableSet *)cp_mutableSetValueForKeyPath:(NSString *)keyPath{
    return nil;
}

- (NSMutableArray *)cp_mutableArrayValueForKeyPath:(NSString *)keyPath{
    return nil;
}

- (NSMutableOrderedSet *)cp_mutableOrderedSetValuesForKeyPath:(NSString *)keyPath{
    return nil;
}

- (BOOL)cp_validateValue:(inout id  _Nullable __autoreleasing *)ioValue forKeyPath:(NSString *)inKeyPath error:(out NSError * _Nullable __autoreleasing *)outError{
    return nil;
}

- (void)cp_setValue:(id)value forUndefinedKey:(NSString *)key{
    NSString *reasoon = [NSString stringWithFormat:@"[%@  cp_setValue:forUndefinedKey:]: this class is not key value coding-compliant for the key %@.",[self description],key];
    
    @throw [NSException exceptionWithName:NSUndefinedKeyException reason:reasoon userInfo:nil];
}

- (id)cp_valueForUndefinedKey:(NSString *)key{
    [self cp_setValue:nil forUndefinedKey:key]; return nil;
}

- (void)cp_setNilValueForKey:(NSString *)key{
    NSString *reason = [NSString stringWithFormat:@"[%@ cp_setNilValueForKey]: could not set nil as the value for the key %@.",[self description],key];
    @throw [NSException exceptionWithName:NSInvalidArgumentException reason:reason userInfo:nil];
}
@end

@implementation NSArray (CPKVC)
- (id)cp_valueForKey:(NSString *)key{
    if (!self.count) return nil;
    return nil;
}

- (void)cp_setValue:(id)value forKey:(NSString *)key{
    return;
    
}
@end

@implementation NSDictionary (CPKVC)

- (id)cp_valueForKey:(NSString *)key{
    return nil;
}
@end

@implementation NSMutableDictionary (CPKVC)

- (void)cp_setValue:(id)value forKey:(NSString *)key{
    
}
@end

@implementation NSOrderedSet (CPKVC)

- (id)cp_valueForKey:(NSString *)key{
    return nil;
}

- (void)cp_setValue:(id)value forKey:(NSString *)key{
    
}
@end

@implementation NSSet (CPKVC)

- (id)cp_valueForKey:(NSString *)key{
    return nil;
}

- (void)cp_setValue:(id)value forKey:(NSString *)key{
    
}
@end

@implementation NSObject (CPDeprecatedKVC)

@end
