//
//  NSObject+CPKVC.h
//  KVCImplementation
//
//  Created by near on 2017/7/24.
//  Copyright © 2017年 near. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/* 键值编码操作失败时抛出的异常。 异常的用户信息字典将至少包含两个条目:
 - @"NSTargetObjectUserInfoKey": KVC消息失败的接收者.
 - @"NSUnknownUserInfoKey": 在失败的KVC消息中使用的密钥.
 
 该常量字符串的实际值为“NSUnknownKeyException”，以匹配在Mac OS 10.3中不推荐使用的KVC方法抛出的异常.
 */

FOUNDATION_EXPORT NSExceptionName const CP_NSUndefinedKeyException;

typedef NSString * CP_NSKeyValueOperator NS_STRING_ENUM;
/* 字符串，用于键值编码支持的数组运算符的名称。 只有这些字符串声明在Mac OS 10.4中是新的。 数组运算符的实际支持出现在Mac OS 10.3中。 这些值不包括“@”前缀.
 */
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSAverageKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSCountKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSDistinctUnionOfArraysKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSDistinctUnionOfObjectsKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSDistinctUnionOfSetsKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSMaximumKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSMinimumKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSSumKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSUnionOfArraysKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSUnionOfObjectsKeyValueOperator;
FOUNDATION_EXPORT NSKeyValueOperator const CP_NSUnionOfSetsKeyValueOperator;
@interface NSObject (CP_NSKeyValueCoding)
/*
 返回YES如果-valueForKey :, -setValue：forKey :, -mutableArrayValueForKey :, -storedValueForKey :, -takeStoredValue：forKey :,和-takeValue：forKey：发送到接收类的实例时，可以直接操作实例变量，否则为否。 此属性的默认实现返回YES.
 */

#if FOUNDATION_SWIFT_SDK_EPOCH_AT_LEAST(8)
@property (class, readonly) BOOL cp_accessInstanceVariableDirectly;
#endif
/* 给定一个值和一个标识属性的键，设置属性的值。给定一个对象和一个标识一个到一个关系的键，将该对象与接收方联系起来，如果有一个对象，则将先前相关的对象关联起来。给定一个集合对象和一个标识一个到许多关系的键，将集合中包含的对象与接收方关联起来，如果有任何关系，则将之前相关的对象关联起来
 该方法的默认实现如下:
 1. 在接收方的类中搜索一个访问器方法，该方法的名称为 -set<Key>:.如果找到这样的方法，则检查其参数的类型.如果参数类型不是一个对象指针类型但值为nil -setNilValueForKey:被调用.默认实现 -setNilValueForKey: 的一个NSInvalidArgumentException,但是您可以在您的应用程序上重写.否则，如果方法的参数类型是一个对象指针类型该方法就被简单地用值作为参数调用.如果方法的参数类型是另一种类型，则是在调用方法之前执行由 -valueForKey: 执行的 NSNumber/NSValue 转换的逆函数:.
 2. 否则(没有找到访问器方法),如果接收方的类 +accessInstanceVariablesDirectly属性返回YES,按这种顺序搜索接收者的类的一个实例变量名称相匹配的 _<key>, _is<Key>, <key>, 或者 is<Key>.如果找到了这样一个实例变量，并且它的类型是一个对象指针类型，那么该值将被保留，并且在实例变量的旧值被首次释放后，其结果被设置在实例变量中.如果实例变量的类型是另一种类型它的值是在从NSNumber或NSValue转换为第1步的类型之后设置的.
 3. 否则(没有找到访问器方法或实例变量),调用 -setvalue:forUndefinedKey:.默认实现的setvalue:forUndefinedKey: 的一个 NSUndefinedKeyException ,但是您可以在您的应用程序上重写
 
 Compatibility notes:
    - 对于向后的二进制兼容性与 -takeValue:forKey:行为，一个名称与模式相匹配的方法 -_set<Key>:在步骤1中也被识别。KVC访问器的名字从下划线开始，但在Mac OS 10.3中被弃用.
    - 对于向后的二进制兼容性，-unableToSetNilForKey: 将被调用，而不是 -setNilValueForKey: 在步骤1中，如果在接收者的类中不是NSObject实现 -unableToSetNilForKey: .
    - 步骤2中描述的行为不同于 -takeValue:forKey: 在其中，实例变量搜索顺序是 <key>, _<key>.
    - 为了向后二进制兼容性将调用行为 -takeValue:forKey:“,在步骤3中 -handleTakeValue:forUnboundKey: 代替 setvalue:forUndefinedKey: ,如果不是NSObject接收机的类实现 -handleTakeValue:forUnboundKey:.
 */
- (void)cp_setValue:(nullable id)value forKey:(NSString *)key;

/* 给定一个包含关键属性值，一对一对象和/或多对象对象集合的字典，设置键控值。 其值为NSNull的字典条目导致-setValue：nil forKey：正在发送到接收方的关键消息.
 */
- (void)cp_setValuesForKeysWithDictionary:(NSDictionary<NSString *, id> *)keyedValues;

/* 给定一个标识属性或一对一关系的键，返回属性值或相关对象。 给出一个标识一对多关系的键，返回一个不可变数组或一个包含所有相关对象的不可变集合
 该方法的默认实现如下所做：
 1.按照该顺序搜索名称匹配模式-get <Key>， - <key>或-is <Key>的访问器方法的接收者类。 如果发现这样的方法被调用。 如果方法的结果类型是一个对象指针类型，则返回结果。 如果结果的类型是NSNumber转换支持的标量类型之一，并返回NSNumber。 否则，转换完成并返回一个NSValue（Mac OS 10.5中的新值：任意类型的结果转换为NSValues，而不仅仅是NSPoint，NRange，NSRect和NSSize）.
 2.(在Mac OS 10.7中引入)否则(不存在简单的访问器方法),在接收方的类中搜索名称与模式的名称匹配的,-countOf<Key>,-indexIn<Key>OfObject:,-objectIn<Key>AtIndex:(与NSOrderedSet类的原始方法相对应)以及 -<key>AtIndexes:(对应于-[NSOrderedSet objectsAtIndexes:]).如果找到一个count方法和一个indexOf方法，以及其他两个可能的方法中的一个，那么将返回一个响应所有NSOrderedSet方法的集合代理对象.发送到集合代理对象的每个NSOrderedSet消息都会导致一些消息-countOf<Key>, -indexIn<Key>OfObject:, -objectIn<Key>AtIndex:, 和 -<key>AtIndexes:被发送到 -valueForKey:的原始接收者.如果接收方的类也实现了一个可选的方法，它的名称与模式相匹配 -get<Key>:range:将在适合最佳性能时使用.
 3.否则(没有一个简单的访问方法或一组有序的访问方法),在接收方的类中搜索其名称与模式匹配的方法 -countOf<Key> 和 -objectIn<Key>AtIndex: (对应于NSArray类定义的原始方法)和(在Mac OS 10.4中引入)还有 -<key>AtIndexes: (对应于 —[NSArray objectsAtIndexes:]).如果找到一个count方法和其他两种可能的方法中的一个，那么将返回一个响应所有NSArray方法的集合代理对象.发送到集合代理对象的每个NSArray消息都会导致一些消息 -countOf<Key>, -objectIn<Key>AtIndex:, 和 -<key>AtIndexes: 被发送到 -valueForKey:的原始接收者.如果接收方的类也实现了一个可选的方法，与它的名称匹配的 -get<Key>:range: 方法将在适合最佳性能时使用.
 4.(在Mac OS 10.4中引入)否则(没有一个简单的存取器方法或一组有序集或数组访问方法),搜索接收者类的三个方法，这些方法的名称按 -countOf<Key>, -enumeratorOf<Key>, and -memberOf<Key>: 匹配(对应于NSSet类定义的原始方法).如果找到所有三个这样的方法，那么将返回一个响应所有NSSet方法的集合代理对象.发送到集合代理对象的每个NSSet消息都会导致一些消息 -countOf<Key>, -enumeratorOf<Key>, 和 -memberOf<Key>: 被发送到 -valueForKey:的原始接收者.
 5.否则(不存在简单的访问方法或集合访问方法集),如果接收者的类 + accessInstanceVariablesDirectly 属性返回YES,搜索接收机的类的一个实例变量名称这个顺序与 _<key>, _is<Key>, <key>, 或者 is<Key> 匹配.如果找到了这样一个实例变量，那么返回的实例变量的值将返回，与第1步中的NSNumber或NSValue的转换相同.
 6.否则(不存在简单的访问方法、集合访问方法集或实例变量),调用 -valueForUndefinedKey: 并返回结. -valueForUndefinedKey: 默认实现一个NSUndefinedKeyException，但是您可以在您的应用程序中覆盖它.
 
 Compatibility notes:
 - 对于向后的二进制兼容性，可以在步骤1和步骤3之间搜索名为“-_get<Key>”或“-_<key>”的访问器方法.如果找到了这样的方法，它就会被调用，与第1步中的NSNumber或NSValue的转换一样.KVC访问器的名称从下划线开始，但在Mac OS 10.3中被弃用.
 - 第5步中描述的行为与Mac OS 10.2的变化是一样的，其中的实例变量搜索顺序是“<key>”，_<key>.
 - 如果实现 -handleQueryWithUnboundKey: 不是NSObject接收方的类,在步骤6中逆向二进制兼容性的 -handleQueryWithUnboundKey: 将调用,而不是 -valueForUndefinedKey:.
 */
- (nullable id)cp_valueForKey:(NSString *)key;

/* 给定一个标识_unordered_和唯一多对多关系的键，返回一个可提供对相关对象的读写访问的可变集。 添加到可变集中的对象将变得与接收者相关，并且从可变集中删除的对象将变得无关.
 
 此方法的默认实现将识别相同的简单访问器方法，并将访问器方法设置为-valueForKey：'，并遵循相同的直接实例变量访问策略，但始终返回可变集合代理对象，而不是不可变集合-valueForKey： 会回来 它也是:
 1. 搜索名称匹配模式的方法的接收者的类别-add <Key> Object：和-remove <Key> Object：（对应于由NSMutableSet类定义的两个基本方法）以及-add <Key>：和 -remove <Key>：（对应于 - [NSMutableSet unionSet：]和 - [NSMutableSet minusSet：]）。 如果发现至少一个添加方法和至少一个删除方法，则每个发送到集合代理对象的NSMutableSet消息将导致-add <Key> Object :, -remove <Key> Object :, -add <Key> ：和--remove <Key>：发送到-mutableSetValueForKey的原始接收者的消息：。 如果接收者的类也实现了一个可选方法，其名称与pattern -intersect <Key>：或-set <Key>匹配：在适当的情况下，将使用该方法获得最佳性能.
 2. 否则（找不到设置的变异方法），搜索接收者的类别，其名称与pattern -set <Key>：匹配。 如果发现这样一种方法，则发送给收集代理对象的每个NSMutableSet消息将导致一个-set <Key>：消息被发送到-mutableSetValueForKey的原始接收者:.
 3. 否则（没有设置变异方法或简单的访问方法），如果接收者的类'+ accessInstanceVariablesDirectly属性返回为YES，那么搜索接收者的类的名称与模式_ <key>或<key>匹配的实例变量， ， 以该顺序。 如果找到这样一个实例变量，则发送到集合代理对象的每个NSMutableSet消息将被转发到实例变量的值，因此它通常必须是NSMutableSet的实例或NSMutableSet的子类.
 4. 否则（无设置变异方法，简单访问方法或实例变量），返回可变集合代理对象。 发送到集合代理对象的每个NSMutableSet消息将导致一个-setValue：forUndefinedKey：消息被发送到-mutableSetValueForKey的原始接收者。 -setValue：forUndefinedKey的默认实现：引发一个NSUndefinedKeyException，但是您可以在应用程序中覆盖它.
 
 性能说明：重复设置<Key>：第2步描述所暗示的消息是潜在的性能问题。 为了更好的性能实现方法，满足您的KVC兼容类的步骤1的要求.
 */
- (NSMutableSet *)cp_mutableSetValueForKey:(NSString *)key;
/* 给定一个标识_ordered_ 一对多关系的键，返回一个可变数组，它提供对相关对象的读写访问。 添加到可变数组中的对象将变得与接收器相关，并且从可变数组中删除的对象将变得无关.
 
 该方法的默认实现将识别与-valueForKey：相同的简单访问器方法和数组访问器方法，并遵循相同的直接实例变量访问策略，但始终返回可变集合代理对象，而不是不可变的集合-valueForKey： 会回来 它也是:
 1. 搜索接收者的类名称匹配模式的方法-insertObject：在<Key> AtIndex：和-removeObjectFrom <Key> AtIndex :(对应于由NSMutableArray类定义的两个最原始的方法）和（在Mac中引入OS 10.4）还 - 插入<Key>：atIndexes：和-remove <Key> AtIndexes：（对应于 - [NSMutableArray insertObjects：atIndexes：]和 - [NSMutableArray removeObjectsAtIndexes :)。如果发现至少一个插入方法和至少一个删除方法，则发送到集合代理对象的每个NSMutableArray消息将导致-insertObject的某些组合：<Key> AtIndex :, -removeObjectFrom <Key> AtIndex :, -insert < Key>：atIndexes :,和-remove <Key> AtIndexes：发送到-mutableArrayValueForKey的原始接收者的消息。如果接收者的类也实现了名称与模式匹配的可选方法-replaceObjectIn <Key> AtIndex：withObject：或（在Mac OS 10.4中引入）-replace <Key> AtIndexes：使用<Key>：该方法将被使用适合最佳性能.
 2. 否则（找不到数组变量方法），搜索接收者的类名称匹配pattern -set <Key>的访问器方法。 如果发现这样一种方法，则发送给集合代理对象的每个NSMutableArray消息将导致一个-set <Key>：消息被发送到-mutableArrayValueForKey的原始接收者：.
 3. 否则（没有找到数组变量方法或简单的访问方法），如果接收者的类'+ accessInstanceVariablesDirectly属性返回YES'，则会将接收者的类搜索一个与模式_ <key>或<key>匹配的实例变量， ， 以该顺序。 如果找到这样一个实例变量，则发送到集合代理对象的每个NSMutableArray消息将被转发到实例变量的值，因此它通常必须是NSMutableArray的实例或NSMutableArray的子类.
 4. 否则（无一组数组变异方法，简单的访问方法或实例变量），返回一个可变集合代理对象。 发送到集合代理对象的每个NSMutableArray消息将导致一个-setValue：forUndefinedKey：消息被发送到-mutableArrayValueForKey的原始接收者。 -setValue：forUndefinedKey的默认实现：引发一个NSUndefinedKeyException，但是您可以在应用程序中覆盖它.
 
 性能说明：重复设置<Key>：第2步描述所暗示的消息是潜在的性能问题。 为了更好的性能实现插入和删除方法，满足您在KVC兼容类中的步骤1的要求。 为了获得最佳性能，还要实施替代方法.
 */
- (NSMutableArray *)cp_mutableArrayValueForKey:(NSString *)key;
/* 给定一个标识一个_ordered_和唯一的to-many关系的键，返回一个可修改的有序集，提供对相关对象的读写访问。 添加到可变排序集中的对象将变得与接收者相关，并且从可变定序集中删除的对象将变得无关.
 
 此方法的默认实现将同样的简单访问器方法和有序集访问器方法识别为-valueForKey：的，并遵循相同的直接实例变量访问策略，但总是返回可变集合代理对象，而不是不可变的集合-valueForKey ：会回来 它也是:
 1. 搜索名称匹配模式的方法的接收者的类别-insertObject：在<Key> AtIndex：和-removeObjectFrom <Key> AtIndex：（对应于由NSMutableOrderedSet类定义的两个最原始的方法）以及-insert < Key>：atIndexes：和-remove <Key> AtIndexes：（对应于 - [NSMutableOrderedSet insertObjects：atIndexes：]和 - [NSMutableOrderedSet removeObjectsAtIndexes :)。如果发现至少一个插入方法和至少一个删除方法，每个发送到集合代理对象的NSMutableOrderedSet消息将导致-insertObject的某些组合：在<Key> AtIndex :, -removeObjectFrom <Key> AtIndex :, -insert < Key>：atIndexes :,和-remove <Key> AtIndexes：发送到-mutableOrderedSetValueForKey的原始接收者的消息。如果接收者的类也实现了名称与模式匹配的可选方法-replaceObjectIn <Key> AtIndex：withObject：或-replace <Key> AtIndexes：使用<Key>：在适当的情况下将使用该方法获得最佳性能.
 2. 否则（没有找到有序设置变异方法的集合），搜索接收者的类的名称与pattern -set <Key>：匹配的访问器方法。 如果发现这样一种方法，则发送给集合代理对象的每个NSMutableOrderedSet消息将导致将-set <Key>：消息发送到-mutableOrderedSetValueForKey的原始接收者：.
 3. 否则（没有发现一组有序的设置变异方法或简单的访问方法），如果接收者的类'+ accessInstanceVariablesDirectly属性返回为YES，则会在接收者的类中搜索名称匹配模式_ <key>或<key的实例变量 >，按顺序。 如果找到这样一个实例变量，则发送到集合代理对象的每个NSMutableOrderedSet消息将被转发到实例变量的值，因此它通常必须是NSMutableOrderedSet的实例或NSMutableOrderedSet的子类.
 4. 否则（没有设置有序的设置变异方法，简单的访问方法或实例变量），返回一个可变的集合代理对象。 发送到集合代理对象的每个NSMutableOrderedSet消息将导致一个-setValue：forUndefinedKey：消息被发送到-mutableOrderedSetValueForKey的原始接收者。 -setValue：forUndefinedKey的默认实现：引发一个NSUndefinedKeyException，但是您可以在应用程序中覆盖它.
 
 Performance note: 重复的 -set<Key>:步骤2的描述所暗示的消息是潜在的性能问题。为了实现更好的性能，实现插入和删除方法，这些方法满足了符合 kvc 的类中的步骤1的要求. 为了实现最佳性能，也要实现一个替代方法.
 */
- (NSMutableOrderedSet *)cp_mutableOrderedSetValuesForKey:(NSString *)key;
/* 给定一组key，返回一个包含键属性值的字典，一个相关的对象或多相关对象的集合。条目使用-valueForKey:返回的字典中具有NSNull，返回nil.
 */
- (NSDictionary<NSString *, id> *)cp_dictionaryWithValuesForKeys:(NSArray<NSString *> *)keys;

/* 给定一个指向一个值指针的指针，一个标识一个属性或一个关系的键，一个指向NSError指针的指针，返回一个值，这个值适合于随后的-setValue:forKey:发送给同一接收者的消息. 如果没有必要的验证，返回YES，而不改变 *ioValue 或 *outError. 如果验证是必要且可能的，那么在将*ioValue设置为已验证的原始值的对象之后，返回YES，但不需要改变 *outError. 如果验证是必要的，但不可能的，在将 *outError 设置为NSError后，返回NO，它封装了验证不可能的原因，但不改变 *ioValue. 消息的发送者永远不会为 *ioValue 或 *outError 而承担责任。.
 
 这个方法的默认实现是搜索一个验证器方法的类，该方法的名称是 -validate:error:. 如果找到了这样的方法，就会调用该方法并返回结果. 如果没有找到这样的方法,就会返回YES.
 */
- (BOOL)cp_validateValue:(inout id _Nullable * _Nonnull)ioValue forKey:(NSString *)inKey error:(out NSError **)outError;

/* Key-path-taking 变量同名的子对象的方法.每个组件的默认实现都将解析关键路径，以确定其是否具有多个组件(关键路径组件按时间间隔分隔).如果是这样的话，-valueForKey:被第一个键路径组件作为参数调用，并且被调用的方法在结果上被递归调用，其余的键路径作为参数传递.如果不是，就会调用与 non-key-path-taking 命名方法相同的方法
 */
- (void)cp_setValue:(nullable id)value forKeyPath:(NSString *)keyPath;
- (nullable id)cp_valueForKeyPath:(NSString *)keyPath;
- (NSMutableSet *)cp_mutableSetValueForKeyPath:(NSString *)keyPath;
- (NSMutableArray *)cp_mutableArrayValueForKeyPath:(NSString *)keyPath;
- (NSMutableOrderedSet *)cp_mutableOrderedSetValuesForKeyPath:(NSString *)keyPath;
- (BOOL)cp_validateValue:(inout id _Nullable * _Nonnull)ioValue forKeyPath:(NSString *)inKeyPath error:(out NSError **)outError;

/* 假定调用 -cp_setValue:forKey:无法使用其默认机制来设置key，使用其他机制设置key.该方法的默认实现了一个NSUndefinedKeyException.你可以重写它以处理在运行时动态定义的属性
 */
- (void)cp_setValue:(nullable id)value forUndefinedKey:(NSString *)key;
/* 假定调用-cp_valueForKey: 无法使用其默认访问机制获得key,使用其他机制返回key.该方法的默认实现了一个NSUndefinedKeyException.你可以重写它以处理在运行时动态定义的属性
 */
- (nullable id)cp_valueForUndefinedKey:(NSString *)key;

/* 由于调用 -cp_setValue:forKey:无法设置key，因为对应的访问器方法的参数类型是值为nil的NSNumber标量类型或NSValue结构类型，使用其他机制设置key。该方法的默认实现抛出一个NSInvalidArgumentException。你可以重写它将nil值映射到应用程序的上下文中有意义的东西
 */
- (void)cp_setNilValueForKey:(NSString *)key;

@end


@interface NSArray<ObjectType> (CP_NSKeyValueCoding)
/* 在每个接收者的元素上返回一个包含调用-valueForKey结果的数组。 返回的数组将包含-valueForKey的每个实例的NSNull元素：返回nil.
 */
- (id)cp_valueForKey:(NSString *)key;

/* 在每个接收方的成员上调用-setValue：forKey：.
 */
- (void)cp_setValue:(nullable id)value forKey:(NSString *)key;

@end

@interface NSDictionary <KeyType, ObjectType>(CPKVC)

/* 返回 -objectForKey: 的结果
 */
- (nullable ObjectType)cp_valueForKey:(NSString *)key;

@end

@interface NSMutableDictionary <KeyType, ObjectType>(CPKVC)

/* 给接收者发送 -cp_setObject:forKey,除非值为nil,在这种情况下发送 -removeObjectForKey:.
 */
- (void)cp_setValue:(nullable ObjectType)value forKey:(NSString *)key;

@end

@interface NSOrderedSet <ObjectType>(CP_NSKeyValueCoding)
/* 在每个接收者的成员上返回一个包含调用-valueForKey的结果的有序集合。 返回的有序集可能不具有与接收者相同数量的成员。 返回的有序集将不包含与-valueForKey实例相对应的任何元素：返回nil，也不会包含重复项.
 */
- (id)cp_valueForKey:(NSString *)key;
/* 在每个接收方的成员上调用-setValue：forKey：.
 */
- (void)cp_setValue:(id)value forKey:(NSString *)key;

@end

@interface NSSet <ObjectType>(CP_NSKeyValueCoding)

/* 在每个接收者的成员上返回一个包含调用-valueForKey的结果的集合。 返回的集合可能不具有与接收方相同数量的成员。 返回的集合将不包含对应于-valueForKey实例的任何元素：返回nil（与 - [NSArray（NSKeyValueCoding）valueForKey：]相对照，这可能会使NSNulls返回到数组中）
 */
- (id)cp_valueForKey:(NSString *)key;
/* 在每个接收方的成员上调用-setValue：forKey：.
 */
- (void)cp_setValue:(id)value forKey:(NSString *)key;

@end


@interface NSObject (CP_NSDeprecatedKeyValueCoding)
/* Mac OS 10.4中已弃用的方法.
 */
+ (BOOL)cp_useStoredAccessor NS_DEPRECATED(10_0, 10_4, 2_0, 2_0);
- (nullable id)cp_storedValueForKey:(NSString *)key NS_DEPRECATED(10_0, 10_4, 2_0, 2_0);
- (void)cp_takeStoredValue:(nullable id)value forKey:(NSString *)key NS_DEPRECATED(10_0, 10_4, 2_0, 2_0);

/* 在Mac OS 10.3中已弃用的方法。 使用上面声明的新的，更一致的命名方法.
 */
- (void)cp_takeValue:(nullable id)value forKey:(NSString *)key NS_DEPRECATED(10_0, 10_3, 2_0, 2_0);
- (void)cp_takeValue:(nullable id)value forKeyPath:(NSString *)keyPath NS_DEPRECATED(10_0, 10_3, 2_0, 2_0);
- (nullable id)cp_handleQueryWithUnboundKey:(NSString *)key NS_DEPRECATED(10_0, 10_3, 2_0, 2_0);
- (void)cp_handleTakeValue:(nullable id)value forUnboundKey:(NSString *)key NS_DEPRECATED(10_0, 10_3, 2_0, 2_0);
- (void)cp_unableToSetNilForKey:(NSString *)key NS_DEPRECATED(10_0, 10_3, 2_0, 2_0);
- (NSDictionary *)cp_valuesForKeys:(NSArray *)keys NS_DEPRECATED(10_0, 10_3, 2_0, 2_0);
- (void)cp_takeValuesFromDictionary:(NSDictionary *)properties NS_DEPRECATED(10_0, 10_3, 2_0, 2_0);
@end
NS_ASSUME_NONNULL_END
