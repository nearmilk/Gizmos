//
//  main.m
//  KVCAndKVOImplementation
//
//  Created by near on 2018/1/16.
//  Copyright © 2018年 near. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
